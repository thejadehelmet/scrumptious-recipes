from django.db import models
from django.conf import settings


# Create your models here.


class MealPlan(models.Model):
    name = models.CharField(max_length=120)
    date = models.DateField()
    owner = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        related_name=("meal_plan"),
        on_delete=models.CASCADE,
        null=True,
    )
    recipes = models.ManyToManyField(
        "recipes.Recipe",
        related_name="recipes",
    )

    def __str__(self):
        return self.name
