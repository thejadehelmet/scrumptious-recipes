from django import template

register = template.Library()


def resize_to(ingredient, target):
    number_servings = ingredient.recipe.servings
    if number_servings is not None and target is not None:
        ratio = int(target) / number_servings
        return ratio * ingredient.amount

    return ingredient.amount


register.filter(resize_to)
